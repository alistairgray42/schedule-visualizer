"""
Class definitions etc.
Basically a .h file
"""

from datetime import datetime, timedelta

# Days on which classes are held
weekdays = ["M", "Tu", "W", "Th", "F"]

# Times at which classes are started, 24-hour
allowed_times = [str(hour) + ":00" for hour in range(8, 21)]

class Meeting:
    """
    Represents all possible information about a class meeting; much here is not used
    Attributes: course e.g. "MATH 109"
                section e.g. "A00"
                type e.g. "LE"
                building e.g. "WLH"
                room e.g. "2204"
                days e.g. ["Tu", "Th"]
                start_time e.g. "14:00"
                end_time e.g. "14:50"
                instructor e.g. "Staff"
    """
    def __init__(self, course, type):
        self.course, self.type, self.name = course, type, f"{course} {type}"

    # MATH 109 AO1 DI; WLH 2204; Tu, Th 14:00 - 14:50; Staff
    def __repr__(self):
        return (f"{self.course} {self.section} {self.type}; {self.building} {self.room}; {', '.join(self.days)} "
                f"{self.start_time} - {self.end_time}; {self.instructor}")


class Schedule:
    """
    Attributes: days: list of Days
    """
    def __init__(self):
        self.days = []


class Day:
    """
    Attributes: name e.g. "Monday"
                classes: list of Classes
    """
    def __init__(self, name):
        self.name = name
        self.classes = []


class Class:
    """
    Attributes: name e.g. "CSE 11"
                height: percentage height
                top: percentage from top of column
    """

    @staticmethod
    def start_time_to_position(time: str):
        hours = int(time[:time.find(':')]) - 7

        if time[-2] == '3':
            return 10 + 6.2 * hours
        else:
            return 7 + 6.2 * hours

    @staticmethod
    def round_up_to_30s(num: int):
        if num % 30 == 0:
            return num
        else:
            return 30 * (num // 30) + 30

    @staticmethod
    def duration_to_height(length: int):
        length = Class.round_up_to_30s(length)

        if length % 60 == 0:
            return 5 + 6.2 * (length - 60) / 60
        else:
            return 2 + 6.2 * (length - 30) / 60

    @staticmethod
    def boundaries_to_height(start, end):
        # uses given start and end time to return a valid length with the datetime module
        start_time = datetime.strptime(start, "%H:%M")
        end_time = datetime.strptime(end, "%H:%M")
        duration = (end_time - start_time) / timedelta(seconds=60)
        return Class.duration_to_height(duration)


    def __init__(self, meeting: Meeting):
        self.name = meeting.name
        self.top = Class.start_time_to_position(meeting.start_time)
        self.height = Class.boundaries_to_height(meeting.start_time, meeting.end_time)


