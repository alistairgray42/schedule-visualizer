import sqlite3
from src.common import *
import os.path


def process_time(time):
    hours, minutes = time[:-1].split(':')
    hours = int(hours)

    if time[-1] == 'p' and time[:2] != "12":
        hours += 12

    hours = str(hours)

    if len(hours) == 1:
        hours = "0" + hours

    return "{}:{}".format(hours, minutes)


def process_days(days):
    return [day for day in weekdays if day in days]


def read(quarter, weekday="%", building="%", room="%"):
    """
    Reads from the specified database, returning a list of Meeting objects representing matching hits
    """
    print(os.path.join(os.path.dirname(__file__), f"../data/{quarter}.db"))
    connection = sqlite3.connect(os.path.join(os.path.dirname(__file__), f"../data/{quarter}.db"))
    cursor = connection.cursor()

    query = 'SELECT * FROM times WHERE days LIKE "{}" AND building LIKE "{}" AND room LIKE "{}"'

    cursor.execute(query.format(weekday, building, room))

    results = []

    for row in cursor:
        meeting = Meeting(row[1], row[2])

        meeting.section = row[3]
        meeting.building, meeting.room, meeting.instructor = row[6], row[7], row[8].replace('\t', '').replace('\n', '')

        meeting.start_time, meeting.end_time = map(process_time, row[5].split('-'))

        meeting.days = process_days(row[4])

        results.append(meeting)

    return results

