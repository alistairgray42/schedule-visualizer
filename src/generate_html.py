from src.common import *
from jinja2 import Environment, FileSystemLoader, select_autoescape
import os.path


def add_class_to_day(day: Day, class_: Class):
    for existing in day.classes:
        if existing.top == class_.top:
            # merge classes if together
            existing.name += " / " + class_.name
            return

    else:
        day.classes.append(class_)


def generate_structure(meetings):
    """
    Given a list of Meeting objects, generates a Schedule of Day and Class objects
    """
    sched = Schedule()

    sched.days = [Day(day) for day in weekdays]

    for day in sched.days:
        for meeting in meetings:
            if day.name in meeting.days:
                add_class_to_day(day, Class(meeting))

    return sched


def generate_html(sched: Schedule, stylesheet_url=""):
    """
    Given a Schedule, renders a Jinja template with that schedule
    :return:
    """

    # boilerplate from jinja docs: load environment in the templates directory
    env = Environment(
        loader=FileSystemLoader(os.path.join(os.path.dirname(__file__), "../templates")),
        autoescape=select_autoescape(['html', 'xml'])
    )

    # strip whitespace
    env.trim_blocks = True
    env.lstrip_blocks = True
    env.keep_trailing_newline = True

    template = env.get_template('schedule.jinja')

    # with open(os.path.join(os.path.dirname(__file__), "../output/output.html"), "w") as writeFile:
        # writeFile.write(template.render(days=sched.days, room=title))
    return template.render(days=sched.days, stylesheet_url=stylesheet_url)
