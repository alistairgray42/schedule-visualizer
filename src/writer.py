from bs4 import BeautifulSoup
import sqlite3
import json
import os.path


def write_to_db(quarter):
    """
    Reading data from an html file named <quarter>.htm in data/, writes to the corresponding
    database information about class meetings and finals.
    """

    if '-' in quarter:
        quarter = quarter[:quarter.index('-')]
        partial = True
    else:
        partial = False

    connection = sqlite3.connect(f"../public/data/{quarter}.db")
    cursor = connection.cursor()

    create_times = """CREATE TABLE IF NOT EXISTS times (
    id INTEGER PRIMARY KEY,
    course TEXT,
    type CHAR(2),
    section TEXT,
    days TEXT,
    time TEXT,
    building TEXT,
    room TEXT,
    instructor TEXT);"""

    create_finals = """CREATE TABLE IF NOT EXISTS finals (
    id INTEGER PRIMARY KEY,
    course TEXT,
    date TEXT,
    weekday TEXT,
    times TEXT,
    building TEXT,
    room TEXT);"""

    cursor.execute(create_times)
    cursor.execute(create_finals)
    connection.commit()

    # clear out data
    if not partial:
        cursor.execute("DELETE FROM times")
        cursor.execute("DELETE FROM finals")

    # columns are: name+number, type, section, days, time, building, room, instructor
    query = "INSERT INTO times VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)"

    # columns are: name+number, date, weekday, times, building, room
    fiquery = "INSERT INTO finals VALUES (NULL, ?, ?, ?, ?, ?, ?)"

    f = open(os.path.join(os.path.dirname(__file__), f"../public/data/{quarter}.htm"), "r")

    soup = BeautifulSoup(f, 'html.parser')

    # divs in h2 contain subject codes, trs contain other data we want
    rows = soup.find_all({"h2", "tr"})

    subject = ""
    number = ""

    for row in rows:
        if row.name == "h2":
            if '(' in row.text:
                subject = row.text[row.text.find('(') + 1 : row.text.rfind(')') - 1]
            continue

        if "Units" in row.text:
            number = row.find_all("td", class_="crsheader")[1].text
            continue

        # crude way to only get rows that have class meeting data
        if "LE" not in row.text and "DI" not in row.text and "SE" not in row.text \
                and "FI" not in row.text and "ST" not in row.text and "LA" not in row.text \
                and "TU" not in row.text:
            continue

        data = row.find_all("td")

        if len(data) > 8:
            if data[2].text == "FI":
                args = [subject + " " + number, *(item.text.strip() for item in data[3:8])]
                cursor.execute(fiquery, args)
            else:
                args = [subject + " " + number, *(item.text.strip() for item in data[2:9])]
                cursor.execute(query, args)

    connection.commit()


def write_rooms_json():
    """
    Writes to rooms.json a file containing data about which buildings and rooms are in use in each quarter; reads from
    the sqlite .db files previously written to.
    :return:
    """

    to_write = {"s119": {}, "s219": {}, "ss19": {}, "fa19": {}, "wi20": {}, "fa21": {}, "wi22": {}, "sp22": {}}

    for quarter in to_write.keys():
        path = os.path.join(os.path.dirname(__file__), f"../public/data/{quarter}.db")

        connection = sqlite3.connect(path)
        cursor = connection.cursor()

        query = "SELECT building, room FROM times GROUP BY building, room;"

        cursor.execute(query)

        results = [result for result in cursor]
        # now a list of tuples (building, room); we want to group by building

        qresults = dict()
        for result in results:
            # skip TBAs, which aren't buildings
            if result[0] == "TBA":
                continue

            elif result[0] not in qresults:
                qresults[result[0]] = [result[1]]

            else:
                qresults[result[0]].append(result[1])

        to_write[quarter] = qresults

    path = os.path.join(os.path.dirname(__file__), "../public/data/rooms.json")

    with open(path, "w") as openfile:
        json.dump(to_write, openfile)


def write_schedule_json():
    """
    Writes to schedule.json a file containing data about which class meetings are where and when
    :return:
    """
    to_write = {"s119": {}, "s219": {}, "ss19": {}, "fa19": {}, "wi20": {}, "fa21": {}, "wi22": {}, "sp22": {}}

    keys = ("id", "course", "type", "section", "days", "time", "building", "room", "instructor")

    for quarter in to_write.keys():
        path = os.path.join(os.path.dirname(__file__), f"../public/data/{quarter}.db")

        connection = sqlite3.connect(path)
        cursor = connection.cursor()

        query = "SELECT * FROM times;"

        cursor.execute(query)

        to_write[quarter] = [{keys[i]: result[i] for i in range(len(result))} for result in cursor]

    path = os.path.join(os.path.dirname(__file__), "../public/data/schedule.json")

    with open(path, "w") as openfile:
        json.dump(to_write, openfile)

write_to_db("sp22")
write_rooms_json()
write_schedule_json()
# for quarter in ("s119", "s219", "fa19", "wi20", "fa21"):
    # write_to_db(quarter)
    # print(f"done with {quarter}")
