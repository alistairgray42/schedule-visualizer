var resultJSON;
var buildingNamesJSON;
var scheduleJSON;

var qselect;
var bselect;
var rselect;

var weekdays = ["M", "Tu", "W", "Th", "F"];

function convert12to24Hours(time)
{
    // expects a time of format "1:00p"
    if (time[time.length - 1] == "a")
        return time.slice(0, time.length - 1);

    else
    {
        var components = time.slice(0, time.length - 1).split(":");

        if (time.slice(0,2) != "12")
            components[0] = parseInt(components[0]) + 12;

        return components[0] + ":" + components[1];
    }
}

function roundMinutesUpTo30s(num)
{
    var components = num.split(":");
    var length = 60 * parseInt(components[0]) + parseInt(components[1]);

    if (length % 30 == 0)
        return length;
    else
        return length - (length % 30) + 30;
}

function startTimeToPosition(time)
{
    // expects a time of format "13:00"
    var hours = time.split(":")[0] - 7;

    if (time.includes(":30"))
        return 10 + 6.2 * hours;
    
    else
        return 7 + 6.2 * hours;
}

function boundariesToHeight(start, end)
{
    // expects two times of format "13:00"
    var length = roundMinutesUpTo30s(end) - roundMinutesUpTo30s(start);

    if (length % 60 == 0)
        return 5 + 6.2 * (length - 60) / 60;
    else
        return 2 + 6.2 * (length - 30) / 60;
}

function JSONtoHTML(json)
{
    var ret = $("<div class=\"class\">");

    var times = json["time"].split("-");
    times[0] = convert12to24Hours(times[0]);
    times[1] = convert12to24Hours(times[1]);

    ret.text(json["course"] + " " + json["type"]);

    ret.css("top", startTimeToPosition(times[0]) + "%");
    ret.css("height", boundariesToHeight(times[0], times[1]) + "%");

    return ret;
}

function setSchedule()
{
    scheduleJSON[qselect.val()].forEach(function(element){
        if (element["building"] == bselect.val() && element["room"] == rselect.val())
        {
            for (var i = 0; i < 5; i++)
            {
                if (element["days"].includes(weekdays[i]))
                {
                    var toAppend = JSONtoHTML(element);
                    var duplicates = $("#day-" + weekdays[i] + " > div[style*='top: " + toAppend.css("top") + "']");

                    // check if one at the same position is already there
                    if (duplicates.length == 0)
                    {
                        $("#day-" + weekdays[i]).append(toAppend);
                    }
                    else
                    {
                        duplicates[0].textContent += " / " + toAppend.text();
                    }
                }
            }
        }
    });
}

function clearSchedule()
{
    for (var i = 0; i < 5; i++)
    {
        $("#day-" + weekdays[i] + " > .class").remove();
    }
}

$(document).ready(function(){

    qselect = $("#quarter-select");
    bselect = $("#building-select");
    rselect = $("#room-select");

    // qselect.val("");
    // bselect.val("");
    // rselect.val("");

    $.getJSON("data/rooms.json", function (result) {
        resultJSON = result;
    });

    $.getJSON("data/building-names.json", function (result) {
        buildingNamesJSON = result;
    });

    $.getJSON("data/schedule.json", function (result) {
        scheduleJSON = result;
    });

    qselect.change(function(){
        // when quarter is selected, populate building dropdown
        bselect.empty();
        bselect.append($("<option />").text("Select Building").val("default"));

        if (qselect.val() != "default")
        {
            $.each(Object.keys(resultJSON[qselect.val()]), function () {
                // https://stackoverflow.com/questions/815103/jquery-best-practice-to-populate-drop-down
                bselect.append($("<option />").text(buildingNamesJSON[this]).val(this));
            });
        }

        bselect.val("default");
        rselect.val("default");
        clearSchedule();
    });

    bselect.change(function(){
        rselect.empty();
        rselect.append($("<option />").text("Select Room").val("default"));

        if (bselect.val() != "default")
        {
            $.each(resultJSON[qselect.val()][bselect.val()], function() {
                rselect.append($("<option />").text(this).val(this));
            });
        }

        rselect.val("default");
        clearSchedule();
    });

    rselect.change(function()
    {
        /* display schedule */
        $("#content")[0].style.display = "flex";
        clearSchedule();
        setSchedule();
    });
})
