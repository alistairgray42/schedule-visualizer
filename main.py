#!venv/bin/python

from flask import Flask, url_for
from flask_cors import CORS

from src.reader import *
from src.writer import *
from src.generate_html import *

"""
app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'
CORS(app)

# results = read("fa19", building="WLH", room="2204")

# sched = generate_structure(results)
# generate_html(sched, "Warren 2204")


@app.route('/<quarter>/<building>/<room>')
def generate_schedule(quarter="", building="", room=""):
    results = read(quarter, building=building, room=room)

    sched = generate_structure(results)

    stylesheet_url = url_for('static', filename='styles/main.css')
    return generate_html(sched, stylesheet_url=stylesheet_url)
"""

write_schedule_json()
